package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
	"unsafe"
)

const (
	STRICT_MODE_PENALTY_GATHER   = 0x100
	MEDIATEK_MODE_PENALTY_GATHER = 0x0304
)

var ITOKEN = MEDIATEK_MODE_PENALTY_GATHER

var ORDER = binary.LittleEndian

type Parcel struct {
	b []byte

	reader *bytes.Buffer
}

func ParcelNew() *Parcel {
	return new(Parcel)
}

func ParcelData(buf uintptr, s int) *Parcel {
	p := &Parcel{}
	p.SetData(buf, s)
	return p
}

func (m *Parcel) SetData(buf uintptr, s int) {
	m.b = (*[1 << 30]byte)(unsafe.Pointer(buf))[:s]
	m.reader = bytes.NewBuffer(m.b)
}

func (p *Parcel) Bytes() []byte {
	return p.b
}

func (p *Parcel) Len() int {
	return len(p.b)
}

func (p *Parcel) Ptr() uintptr {
	if p.Len() == 0 {
		return 0
	}
	return uintptr(unsafe.Pointer(&p.b[0]))
}

func (p *Parcel) ReplyAlloc(size int) {
	p.b = make([]byte, size)
}

func (p *Parcel) ReplyDone(size int) {
	p.b = p.b[:size]
	p.reader = bytes.NewBuffer(p.b)
}

func (p *Parcel) WriteInterfaceToken(s string) {
	p.WriteInt32(int32(ITOKEN))
	p.WriteString(s)
}

func (p *Parcel) WriteString(s string) {
	bb := []byte(s)

	bb = append(bb, 0)

	p.WriteInt32(int32(len(s)))
	for i := 0; i < len(bb); i++ {
		p.b = append(p.b, bb[i])
		p.b = append(p.b, 0)
	}

	p.align(4)
}

func (p *Parcel) align(n int) {
	mod := len(p.b) % n
	if mod == 0 {
		return
	}
	for i := 0; i < (n - mod); i++ {
		p.b = append(p.b, 0)
	}
}

func (p *Parcel) WriteInt32(v int32) {
	p.align(4)
	var buf [4]byte
	*(*int32)(unsafe.Pointer(&buf[0])) = v
	p.b = append(p.b, buf[:]...)
}

func (m *Parcel) EOF() bool {
	return !(m.reader.Len() > 0)
}

func (m *Parcel) ReadStruct(data interface{}) error {
	return binary.Read(m.reader, ORDER, data)
}

func (r *Parcel) ReadInt32() (int32, error) {
	var i int32
	err := binary.Read(r.reader, ORDER, &i)
	return i, err
}

func (r *Parcel) ReadString() (string, error) {
	size, err := r.ReadInt32()
	if err != nil {
		return "", err
	}
	if size == -1 {
		// Map java null strings to the empty string.
		return "", nil
	}
	if size > 1<<20 {
		fmt.Fprintf(os.Stderr, "bug? large string being received: %d bytes\n", size)
	}
	var buf = make([]byte, size)
	r.reader.Read(buf)

	return string(buf), nil
}
