package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"syscall"
	"unsafe"
)

// from binder.h
//
// binder_transaction_data struct
//

type binder_transaction_data_target uint32

func (m *binder_transaction_data_target) handle(u int) {
	*m = binder_transaction_data_target(u)
}

func (m *binder_transaction_data_target) ptr(u uint32) {
	*m = binder_transaction_data_target(u)
}

type binder_transaction_data_data [8]byte

func (m *binder_transaction_data_data) buf(b []byte) {
	for i, x := range b {
		m[i] = byte(x)
	}
}

func (m *binder_transaction_data_data) SetBuffer(buffer uintptr) {
	ORDER.PutUint32(m[:4], uint32(buffer))
}

func (m *binder_transaction_data_data) SetOffset(offsets uintptr) {
	ORDER.PutUint32(m[4:], uint32(offsets))
}

func (m *binder_transaction_data_data) GetBuffer() uintptr {
	return uintptr(ORDER.Uint32(m[:4]))
}

func (m *binder_transaction_data_data) GetOffset() uintptr {
	return uintptr(ORDER.Uint32(m[4:]))
}

type binder_transaction_data struct {
	Target       binder_transaction_data_target
	Cookie       uint32
	Code         uint32
	Flags        uint32
	Sender_pid   int32
	Sender_euid  int32
	Data_size    int32
	Offsets_size int32
	Data         binder_transaction_data_data
}

type binder_write_read struct {
	Write_size     int
	Write_consumed int
	Write_buffer   uintptr
	Read_size      int
	Read_consumed  int
	Read_buffer    uintptr
}

const (
	FIRST_CALL_TRANSACTION    = 1
	GET_SERVICE_TRANSACTION   = FIRST_CALL_TRANSACTION + 0
	CHECK_SERVICE_TRANSACTION = FIRST_CALL_TRANSACTION + 1
	ADD_SERVICE_TRANSACTION   = FIRST_CALL_TRANSACTION + 2
	LIST_SERVICES_TRANSACTION = FIRST_CALL_TRANSACTION + 3
)

const (
	TF_ONE_WAY     = 0x1
	TF_ROOT_OBJECT = 0x4
	TF_STATUS_CODE = 0x8
	TF_ACCEPT_FDS  = 0x10
)

type binder_version struct {
	protocol_version int
}

type binder_pri_ptr_cookie struct {
	priority int
	ptr      uintptr
	cookie   uintptr
}

var BINDER_WRITE_READ = _IOWR('b', 1, int(unsafe.Sizeof(binder_write_read{})))
var BINDER_VERSION = _IOWR('b', 9, int(unsafe.Sizeof(binder_version{})))
var BINDER_SET_MAX_THREADS = _IOW('b', 5, int(unsafe.Sizeof(int(0))))

var BC_TRANSACTION = _IOW_BAD('c', 0, int(unsafe.Sizeof(binder_transaction_data{})))
var BC_ACQUIRE = _IOW_BAD('c', 5, int(unsafe.Sizeof(int(0))))     // 0x40046305
var BC_INCREFS = _IOW_BAD('c', 4, int(unsafe.Sizeof(int(0))))     //0x40046304
var BC_FREE_BUFFER = _IOW_BAD('c', 3, int(unsafe.Sizeof(int(0)))) //0x40046303

var PING_TRANSACTION = int(('_' << 24) | ('P' << 16) | ('N' << 8) | 'G')

const BINDER_VM_SIZE = ((1 * 1024 * 1024) - (4096 * 2))

var BR_TRANSACTION_COMPLETE = _IO('r', 6)
var BR_DEAD_REPLY = _IO('r', 5)
var BR_FAILED_REPLY = _IO('r', 17)
var BR_ACQUIRE_RESULT = _IOR_BAD('r', 4, int(unsafe.Sizeof(int(0))))
var BR_REPLY = _IOR_BAD('r', 3, int(unsafe.Sizeof(binder_transaction_data{})))
var BR_ERROR = _IOR_BAD('r', 0, int(unsafe.Sizeof(int(0))))
var BR_OK = _IO('r', 1)
var BR_TRANSACTION = _IOR_BAD('r', 2, int(unsafe.Sizeof(binder_transaction_data{})))
var BR_NOOP = _IO('r', 12)
var BR_FINISHED = _IO('r', 14)
var BR_ATTEMPT_ACQUIRE = _IOR_BAD('r', 11, int(unsafe.Sizeof(binder_pri_ptr_cookie{})))

type Binder struct {
	file *os.File
	mmap []byte
}

func (m *Binder) Open() error {
	var err error

	m.file, err = os.OpenFile("/dev/binder", os.O_RDWR, 0)
	if err != nil {
		return err
	}

	m.mmap, err = syscall.Mmap(int(m.file.Fd()), 0, BINDER_VM_SIZE, syscall.PROT_READ, syscall.MAP_PRIVATE|syscall.MAP_NORESERVE)
	if err != nil {
		return err
	}

	return nil
}

func (m *Binder) Close() error {
	err := syscall.Munmap(m.mmap)
	if err != nil {
		return err
	}
	err = m.file.Close()
	if err != nil {
		return err
	}

	return nil
}

func (m *Binder) Acquire(i int) error {
	err := m.command(BC_INCREFS, uintptr(i))
	if err != nil {
		return err
	}

	err = m.command(BC_ACQUIRE, uintptr(i))
	if err != nil {
		return err
	}

	return nil
}

func (m *Binder) FreeBuffer(ptr uintptr) error {
	err := m.command(BC_FREE_BUFFER, ptr)
	if err != nil {
		return err
	}

	return nil
}

func (m *Binder) Version() int {
	var vers int
	err := ioctl(m.file.Fd(), BINDER_VERSION, uintptr(unsafe.Pointer(&vers)))
	if err != nil {
		panic(err)
	}
	return vers
}

func (m *Binder) SetMaxThreads(max int) {
	err := ioctl(m.file.Fd(), BINDER_SET_MAX_THREADS, uintptr(unsafe.Pointer(&max)))
	if err != nil {
		panic(err)
	}
}

func dumpbuf(buf uintptr, size int) string {
	if buf == 0 {
		return "nil"
	}

	str := fmt.Sprintf("[0x%08x, %d] ", buf, size)

	bb := (*[1 << 30]byte)(unsafe.Pointer(buf))[:size]
	for i := range bb {
		str += fmt.Sprintf("%02x ", bb[i])
	}

	return str
}

func (m *Binder) Ping() error {
	data := ParcelNew()
	reply := ParcelNew()
	err := m.transact(0, PING_TRANSACTION, data, reply)
	if err != nil {
		return err
	}
	i, err := reply.ReadInt32()
	if err != nil {
		return err
	}
	fmt.Println("ping:", i)

	return nil
}

func (m *Binder) transact(target int, code int, data *Parcel, reply *Parcel) error {
	var bwr binder_write_read

	var wbuf []byte
	{
		buf := new(bytes.Buffer)

		err := binary.Write(buf, ORDER, uint32(BC_TRANSACTION))
		if err != nil {
			return err
		}

		var v binder_transaction_data
		v.Target.handle(target)
		v.Code = uint32(code)
		v.Cookie = 0
		v.Flags = TF_ACCEPT_FDS
		v.Sender_pid = 0
		v.Sender_euid = 0

		v.Data_size = int32(data.Len())
		v.Data.SetBuffer(data.Ptr())

		v.Offsets_size = 0
		v.Data.SetOffset(0)

		err = binary.Write(buf, ORDER, v)
		if err != nil {
			return err
		}

		wbuf = buf.Bytes()
	}

	bwr.Write_size = len(wbuf)
	bwr.Write_buffer = uintptr(unsafe.Pointer(&wbuf[0]))

	if reply != nil {
		reply.ReplyAlloc(256)
		bwr.Read_size = reply.Len()
		bwr.Read_buffer = uintptr(reply.Ptr())
	}

	fmt.Printf("binder_transaction_data: % x\n", wbuf)
	fmt.Printf("parcel: % x\n", data.Bytes())

	err := ioctl(m.file.Fd(), BINDER_WRITE_READ, uintptr(unsafe.Pointer(&bwr)))
	if err != nil {
		return err
	}

	if reply != nil {
		reply.ReplyDone(bwr.Read_consumed)
		fmt.Printf("reply: % x\n", reply.Bytes())
	}

	err = m.transactReply(reply)
	if err != nil {
		return err
	}

	return nil
}

func (m *Binder) transactReply(reply *Parcel) error {
	for !reply.EOF() {
		cmd, err := reply.ReadInt32()
		if err != nil {
			return err
		}
		switch int(cmd) {
		case BR_TRANSACTION_COMPLETE:
			fmt.Println("comlete")
		case BR_DEAD_REPLY:
			fmt.Println("dead")
			return errors.New("dead reply")
		case BR_FAILED_REPLY:
			return errors.New("failed reply")
		case BR_ACQUIRE_RESULT:
			fmt.Println("acq")
		case BR_REPLY:
			fmt.Println("reply")

			var btd binder_transaction_data
			reply.ReadStruct(&btd)

			p := btd.Data.GetBuffer()

			// replace data
			reply.SetData(p, int(btd.Data_size))

			m.FreeBuffer(p)
			return nil
		case BR_ERROR:
			e, err := reply.ReadInt32()
			if err != nil {
				return err
			}
			return errors.New(fmt.Sprintf("%08x", e))
		case BR_OK:
			fmt.Println("ok")
		case BR_TRANSACTION:
			fmt.Println("tran")
		case BR_FINISHED:
			fmt.Println("fin")
		case BR_NOOP:
			fmt.Println("noop")
		default:
			fmt.Println("unknown command", cmd)
		}
	}

	// no more data? and still no transaction complete? read more from binder
	var bwr binder_write_read

	reply.ReplyAlloc(256)
	bwr.Read_size = reply.Len()
	bwr.Read_buffer = reply.Ptr()

	fmt.Println("binder_transaction_data: read")

	err := ioctl(m.file.Fd(), BINDER_WRITE_READ, uintptr(unsafe.Pointer(&bwr)))
	if err != nil {
		return err
	}

	reply.ReplyDone(bwr.Read_consumed)
	fmt.Printf("reply: % x\n", reply.Bytes())

	return m.transactReply(reply)
}

func (m *Binder) command(cmd int, data uintptr) error {
	var bwr binder_write_read

	{
		var wbuf []byte
		buf := new(bytes.Buffer)

		err := binary.Write(buf, ORDER, uint32(cmd))
		if err != nil {
			return err
		}

		err = binary.Write(buf, ORDER, uint32(data))
		if err != nil {
			return err
		}

		wbuf = buf.Bytes()

		bwr.Write_size = len(wbuf)
		bwr.Write_buffer = uintptr(unsafe.Pointer(&wbuf[0]))

		fmt.Printf("command: % x\n", wbuf)
	}

	err := ioctl(m.file.Fd(), BINDER_WRITE_READ, uintptr(unsafe.Pointer(&bwr)))
	if err != nil {
		return err
	}

	return nil
}

func (m *Binder) GetService(name string) (int, error) {
	var data *Parcel = ParcelNew()
	data.WriteInterfaceToken("android.os.IServiceManager")
	data.WriteString(name)

	var reply *Parcel = ParcelNew()

	err := m.transact(0, CHECK_SERVICE_TRANSACTION, data, reply)
	if err != nil {
		return 0, err
	}

	// cmd
	_, err = reply.ReadInt32()
	if err != nil {
		return 0, err
	}

	// magic
	_, err = reply.ReadInt32()
	if err != nil {
		return 0, err
	}

	// handler
	h, err := reply.ReadInt32()
	if err != nil {
		return 0, err
	}

	return int(h), nil
}

func (m *Binder) SendSMS(phone string, msg string) error {
	err := m.Acquire(0)
	if err != nil {
		panic(err)
	}

	target, err := m.GetService("isms")
	if err != nil {
		return err
	}

	err = m.Acquire(target)
	if err != nil {
		panic(err)
	}

	var data *Parcel

	data = ParcelNew()
	data.WriteInterfaceToken("com.android.internal.telephony.ISms")
	data.WriteString(phone)
	data.WriteInt32(-1)
	data.WriteString(msg)
	data.WriteInt32(0)
	data.WriteInt32(0)

	var reply *Parcel
	reply = ParcelNew()

	err = m.transact(target, 5, data, reply)
	if err != nil {
		return err
	}

	i, err := reply.ReadInt32()
	if err != nil {
		return err
	}

	fmt.Println("sms:", reply.Len(), i)
	return nil
}

func (m *Binder) Read() error {
	var bwr binder_write_read

	var reply *Parcel
	reply = ParcelNew()

	reply.ReplyAlloc(256)
	bwr.Read_size = reply.Len()
	bwr.Read_buffer = reply.Ptr()

	fmt.Println("binder_transaction_data: read")

	err := ioctl(m.file.Fd(), BINDER_WRITE_READ, uintptr(unsafe.Pointer(&bwr)))
	if err != nil {
		return err
	}

	reply.ReplyDone(bwr.Read_consumed)
	fmt.Printf("reply: % x\n", reply.Bytes())

	return nil
}
