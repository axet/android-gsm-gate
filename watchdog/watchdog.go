package main

import (
	"fmt"
	"github.com/vaughan0/go-ini"
	"runtime"
)

func main() {
	runtime.LockOSThread()

	file, err := ini.LoadFile("/system/build.prop")
	if err != nil {
		panic(err)
	}
	model, ok := file.Get("", "ro.product.model")
	if !ok {
		panic("no prop found")
	}
	switch model {
	case "KENEKSI-SUN":
		fmt.Println("ken")
	}

	b := &Binder{}

	err = b.Open()
	if err != nil {
		panic(err)
	}

	v := b.Version()
	fmt.Printf("version: %x\n", v)

	b.SetMaxThreads(15)

	err = b.Ping()
	if err != nil {
		panic(err)
	}

	err = b.SendSMS("000100", "b")
	if err != nil {
		panic(err)
	}

	fmt.Println("done")
}
