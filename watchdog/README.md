SMS_RECEIVED

echo sms send 000100 b | nc localhost 5554

// register broadcast incoming sms to ->SmsReceiver
am broadcast -a android.provider.Telephony.SMS_RECEIVED -n com.example.name/.receivers.SmsReceiver

// register SmsReceiver
Context.registerReceiver()

<uses-permission android:name="android.permission.RECEIVE_SMS" />


// http://stackoverflow.com/questions/25969133/how-could-i-get-default-sms-apps-package-name

if(Utils.hasKikKat()) {
    String defaultApplication = Settings.Secure.getString(getContentResolver(),  SMS_DEFAULT_APPLICATION);
    PackageManager pm = context.getPackageManager();
    Intent intent = pm.getLaunchIntentForPackage(defaultApplication );
      if (intent != null) {
        context.startActivity(intent);
      }
} else {
    Intent intent = new Intent(Intent.ACTION_MAIN);
    intent.addCategory(Intent.CATEGORY_DEFAULT);
    intent.setType("vnd.android-dir/mms-sms");
    startActivity(intent);
}

https://github.com/axet/ministrace



// mediatek
service call isms 5 s16 "000100" s16 "b" i32 0 i32 0

/cache/strace -o /sdcard/123 service call isms 5 s16 "000100" i32 -1 s16 "b" i32 0 i32 0
