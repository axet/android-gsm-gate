#!/bin/bash

GOARCH=arm GOOS=linux GOARM=7 go build || exit 1

adb push watchdog /data/local/tmp/
adb shell su -c "/data/local/tmp/watchdog"
