# Keneksi Sun

Sometimes you feel your self connected to your mobile phone number more then it is nessesery. You can avoid accepting voice calls, but you can't ignore sms services connected to your phone number (like mobile banking). Especially when you travaling around the wolrd, but your banks/local services force you to have your mobile in your pocket / online. So keep it online ... at your home.

Now, while you travel, you keep receving / sending SMS from your number but keep your phone at your home.

To let that happens you buy cheap smartphone, install nessesery software and you ready to leave your country and be connected to SS7 nightmare remotelly.

This instruction for model:

  * KENEKSI-SUN
  * armeabi-v7a
  * MT6572
  
You need Superuser software to compile it and create your 'su' binary. You can use propiatary hacks if you wish.

  * https://github.com/axet/Superuser
  
Then find the Mediatek propriatory software (MtkDroidTools v2.5.3 && MediaTek SP Flash Tool 5.13) to write the firmware on the phone (or by the time you gonna read this here will be opensource solutions I hope).

Then compile your SMS routing software SMS->Gmail for example.

  * https://github.com/axet/android-gsm-gate

Short list:

  * root the device (can be done with SuperSU opensource)
  * replace /system/bin/em_svr service with your own watchdog.sh
    * this service is present in /init.rc but mission on the phone system image. you can use it safely. 
  * add service you wana run (sms-backup-plus+)

# Service

watchdog.sh

This service keep wifi working, if phone lose wifi connection (unable to ping default network gateway) it turnoff and turn on the wifi.

Another storey with keneksi-sun - mediatek fimware bugged and eats a lot of memory, so you have to reboot phone peiodically. This service reboots the phone at 03:00 am eveyday.

Tested for a few months - working flawesly.

Depends on your country mobile operator you can setup your phone as a backup internet gateway or use your phone as a asterisk point. Not currently implemented on my end :P

# Network setup

You can setup opensource openwrt sofwate on your router. So you can access your home remotelly using VPN services. With propper software running on your router, you can access your phone using `adb shell` and restart, send sms or do other remote job on it.

Currently to reply to your incoming SMS you just reply it like normal email. sms-gate will check your gmail INBOX folder, parse response and send an SMS.

  * https://github.com/damonkohler/sl4a
  * `am start -n com.zegoggles.smssync/.activity.SendSMS -e phone +199988877766 -e msg "hello"`
  * https://github.com/axet/openwrt-tl-wr842nd-v2
