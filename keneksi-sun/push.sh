#!/bin/bash

adb shell su -c "mount -o remount,rw /system"
adb push watchdog.sh /data/local/tmp/
adb shell su -c "cp /data/local/tmp/watchdog.sh /system/bin/em_svr"
adb shell su -c "chmod 777 /system/bin/em_svr"
echo /system/bin/em_svr
