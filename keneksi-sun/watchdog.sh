#!/system/bin/sh

# reboot ony on satardays, comment it out to reboot every day
#DAY="Sat"
# reboot every 3am
HOUR="03"
# poll every minute (60 seconds)
POLL=60
# for uptime < 3600/86400 - we just rebooted less then one hour ago / day ago, skip reboot
FREEZE=3600

APP=$0

uptime() {
  set -- $(cat /proc/uptime)
  echo ${1%%.*}
}

wifi() {
  DEF=($(ip -4 route list 0/0))
  IP=${DEF[2]}
  ping -q -c1 -w2 $IP > /dev/null
}

wifi_test() {
  wifi
  if [ $? -ne "0" ]; then
    echo "fail"
  else
    echo "success"
  fi
}

reboot_schd() {
  CDAY=$(date +%a)
  CHOUR=$(date +%H)
  UPTIME=$(uptime)
  if [ "$UPTIME" -gt "$FREEZE" ]; then
    if [ -z "$DAY" ] || [ "$CDAY" == "$DAY" ]; then
      if [ "$CHOUR" == "$HOUR" ]; then
        echo "$APP: rebooting..."
        su -c reboot
      fi
    fi
  fi
}

wifi_check() {
  wifi
  RES=$?
  if [ $RES -ne "0" ]; then
    echo "$APP: restarting wifi"
    svc wifi disable
    sleep 2
    svc wifi enable
    echo "$APP: restarting wifi done"
  fi
}

echo "$APP: starting..."

echo "$APP: poll every: $POLL seconds"
echo "$APP: freeze reboot: $FREEZE seconds"
echo "$APP: current uptime: $(uptime)"
echo "$APP: wifi test: $(wifi_test)"

while true; do
  sleep $POLL
  reboot_schd
  wifi_check
done

echo "$APP: done..."