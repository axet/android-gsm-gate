package com.github.axet.smsgate.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.axet.smsgate.R;
import com.github.axet.smsgate.fragments.SchedulersFragment;
import com.zegoggles.smssync.activity.SMSGateFragment;

public class MainActivity extends AppCompatActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;

    OnBackHandler backHandler;

    ViewPager mViewPager;
    TabLayout tabLayout;
    AppBarLayout appbar;
    Handler handler = new Handler();

    public interface OnBackHandler {
        void onBackPressed();
    }

    public static class SettingsTabView extends ImageView {
        Drawable d;

        public SettingsTabView(Context context, TabLayout.Tab tab, ColorStateList colors) {
            super(context);

            d = ContextCompat.getDrawable(context, R.drawable.ic_more_vert_black_24dp);

            setImageDrawable(d);

            setColorFilter(colors.getDefaultColor());
        }

        void updateLayout() {
            ViewParent p = getParent();
            if (p != null && p instanceof LinearLayout) { // TabView extends LinearLayout
                LinearLayout l = (LinearLayout) p;
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) l.getLayoutParams();
                if (lp != null) {
                    lp.weight = 0;
                    lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;

                    int left = l.getMeasuredHeight() / 2 - d.getIntrinsicWidth() / 2;
                    int right = left;
                    left -= l.getPaddingLeft();
                    right -= l.getPaddingRight();
                    if (left < 0)
                        left = 0;
                    if (right < 0)
                        right = 0;
                    setPadding(left, 0, right, 0);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        appbar = (AppBarLayout) findViewById(R.id.appbar);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        setupTabs();
    }

    void setupTabs() {
        tabLayout.setupWithViewPager(mViewPager);
        TabLayout.Tab tab = tabLayout.getTabAt(2);
        SettingsTabView v = new SettingsTabView(this, tab, tabLayout.getTabTextColors());
        tab.setCustomView(v);
        v.updateLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar base clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SMSGateFragment();
                case 1:
                    return new SchedulersFragment();
                case 2:
                    return new Fragment();
                default:
                    throw new RuntimeException("bad page");
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.app_name);
                case 1:
                    return "Schedulers";
                case 2:
                    return "⋮";
                default:
                    throw new RuntimeException("bad page");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (backHandler != null) {
            backHandler.onBackPressed();
            return;
        }
        super.onBackPressed();
    }

    public void onBackHandler(OnBackHandler h) {
        backHandler = h;
        if (h == null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    tabLayout.setTabMode(TabLayout.MODE_FIXED);
                    setupTabs();
                }
            });
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.removeAllTabs();
                final TabLayout.Tab back = tabLayout.newTab();
                back.setCustomView(R.layout.back);
                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        if (tab == back)
                            onBackPressed();
                    }
                });
                tabLayout.addTab(back);
            }
        });
    }
}
