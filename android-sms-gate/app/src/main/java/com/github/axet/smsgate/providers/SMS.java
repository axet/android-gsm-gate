package com.github.axet.smsgate.providers;

import android.os.Build;
import android.telephony.SmsManager;

import com.github.axet.smsgate.mediatek.SmsManagerMT;

/**
 * https://github.com/shutoff/ugona.net/blob/master/src/main/java/net/ugona/plus/Sms.java
 */
public class SMS {
    public static void send(int simID, String phone, String msg) {
        if (Build.VERSION.SDK_INT >= 22) {
            SmsManager sm = SmsManager.getSmsManagerForSubscriptionId(simID);
            sm.sendTextMessage(phone, null, msg, null, null);
            return;
        }

        try {
            SmsManagerMT mt = new SmsManagerMT();
            mt.sendTextMessage(phone, null, msg, null, null, simID);
            return; // metdatek phone
        } catch (Throwable ignore) {
        }
    }

    public static void send(String phone, String msg) {
        SmsManager sm = SmsManager.getDefault();
        sm.sendTextMessage(phone, null, msg, null, null);
    }
}
