package com.github.axet.smsgate.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.provider.Telephony;

import com.github.axet.smsgate.providers.SMS;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.SmsConsts;

import java.util.Date;

/**
 * am start -n com.github.axet.smsgate/.activities.SendSMS -e phone +199988877766 -e msg "hello"
 *
 * am start -n com.github.axet.smsgate/.activities.SendSMS -e phone 000100 -e msg "b"
 */
public class SendSMS extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();

        sendSMS(this, b.getString("phone"), b.getString("msg"), null, null);

        finish();
    }

    public static void sendSMS(Context context, String phone, String msg, Date date, String thread) {
        if (date == null)
            date = new Date();

        ContentResolver res = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SmsConsts.BODY, msg);
        values.put(SmsConsts.ADDRESS, phone);
        values.put(SmsConsts.TYPE, Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT);
        values.put(SmsConsts.PROTOCOL, 0);
        values.put(SmsConsts.SERVICE_CENTER, "");
        values.put(SmsConsts.DATE, date.getTime());
        values.put(SmsConsts.STATUS, -1);
        if (thread != null)
            values.put(SmsConsts.THREAD_ID, thread);
        values.put(SmsConsts.READ, 1);
        res.insert(Consts.SMS_PROVIDER, values);

        SMS.send(phone, msg);
    }
}
