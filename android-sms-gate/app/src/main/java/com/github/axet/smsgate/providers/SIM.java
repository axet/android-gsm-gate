package com.github.axet.smsgate.providers;

import android.content.Context;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;

import com.github.axet.smsgate.mediatek.TelephonyManagerMT;

import java.util.ArrayList;
import java.util.List;

public class SIM {
    List<Integer> sims = new ArrayList<>();
    TelephonyManagerMT mt;
    SubscriptionManager ss;

    public SIM(Context context) {
        if (Build.VERSION.SDK_INT >= 22) {
            ss = SubscriptionManager.from(context);
            List<SubscriptionInfo> ll = ss.getActiveSubscriptionInfoList();
            for (SubscriptionInfo s : ll) {
                sims.add(s.getSubscriptionId());
            }
            return;
        }

        mt = new TelephonyManagerMT(context);
        for (int i = 0; i < 5; i++) {
            if (mt.getSimState(i) != 0) {
                sims.add(i);
            }
        }
    }

    public int getCount() {
        return sims.size();
    }

    public int getSimID(int i) {
        return sims.get(i);
    }

    public String getSerial(int i) {
        if (Build.VERSION.SDK_INT >= 22) {
            int id = sims.get(i);
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getIccId();
        }

        int id = sims.get(i);
        return mt.getSimSerialNumber(id);
    }

    public String getOperatorName(int i) {
        if (Build.VERSION.SDK_INT >= 22) {
            int id = sims.get(i);
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getDisplayName().toString();
        }

        int id = sims.get(i);
        return mt.getSimOperatorName(id);
    }

    public String getOperatorCode(int i) {
        if (Build.VERSION.SDK_INT >= 22) {
            int id = sims.get(i);
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getCarrierName().toString();
        }

        int id = sims.get(i);
        return mt.getSimOperator(id);
    }
}
