package com.github.axet.smsgate.mediatek;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * https://labs.mediatek.com/site/znch/developer_tools/mediatek_android/android_sdk/api_references/mediatek-sdk3/reference/com/mediatek/telephony/TelephonyManagerEx.gsp
 *
 * http://git.huayusoft.com/tomsu/AP7200_MDK-kernel/blob/master/mediatek/frameworks/api/1.txt
 *
 * https://github.com/andr3jx/MTK6577/blob/master/mediatek/source/frameworks/telephony/java/com/mediatek/telephony/TelephonyManagerEx.java
 */
public class TelephonyManagerMT {
    Class sms_class;
    Object m;

    public TelephonyManagerMT(android.content.Context c) {
        try {
            sms_class = Class.forName("com.mediatek.telephony.TelephonyManagerEx");
            Constructor m = sms_class.getConstructor(android.content.Context.class);
            this.m = m.newInstance(c);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int getCallState(int i) {
        return 0;
    }

    public android.telephony.CellLocation getCellLocation(int i) {
        return null;
    }

    public int getDataActivity(int i) {
        return 0;
    }

    public int getDataState(int i) {
        return 0;
    }

    public java.lang.String getDeviceId(int i) {
        return null;
    }

    public java.lang.String getLine1Number(int i) {
        return null;
    }

    public java.util.List<android.telephony.NeighboringCellInfo> getNeighboringCellInfo(int i) {
        return null;
    }

    public java.lang.String getNetworkCountryIso(int i) {
        return null;
    }

    public java.lang.String getNetworkOperator(int i) {
        return null;
    }

    public java.lang.String getNetworkOperatorName(int i) {
        return null;
    }

    public int getNetworkType(int i) {
        return 0;
    }

    public int getPhoneType(int i) {
        return 0;
    }

    public java.lang.String getSimCountryIso(int i) {
        return null;
    }

    public java.lang.String getSimOperator(int i) {
        try {
            Method m = sms_class.getMethod("getSimOperator", int.class);
            return (String) m.invoke(this.m, i);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public java.lang.String getSimOperatorName(int i) {
        try {
            Method m = sms_class.getMethod("getSimOperatorName", int.class);
            return (String) m.invoke(this.m, i);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public java.lang.String getSimSerialNumber(int i) {
        try {
            Method m = sms_class.getMethod("getSimSerialNumber", int.class);
            return (String) m.invoke(this.m, i);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int getSimState(int i) {
        try {
            Method m = sms_class.getMethod("getSimState", int.class);
            return (int) m.invoke(this.m, i);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public java.lang.String getSubscriberId(int i) {
        return null;
    }

    public java.lang.String getVoiceMailAlphaTag(int i) {
        return null;
    }

    public java.lang.String getVoiceMailNumber(int i) {
        return null;
    }

    public boolean hasIccCard(int i) {
        return false;
    }

    public boolean isNetworkRoaming(int i) {
        return false;
    }

    public void listen(android.telephony.PhoneStateListener p, int i, int ii) {
    }
}
