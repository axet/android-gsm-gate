package com.github.axet.smsgate.app;

import android.content.Context;

import com.github.axet.smsgate.providers.SIM;
import com.zegoggles.smssync.App;

public class MainApplication extends App {
    SIM sim;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public SIM getSIM() {
        if(sim == null) {
            sim = new SIM(this);
        }
        return sim;
    }

    public static MainApplication getApp(Context context) {
        return (MainApplication) context.getApplicationContext();
    }
}
