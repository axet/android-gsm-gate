package com.github.axet.smsgate.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v13.app.FragmentCompat;
import android.support.v14.preference.PreferenceFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.activities.MainActivity;
import com.github.axet.smsgate.dialogs.EditDialogFragment;

/**
 */
public class SchedulersFragment extends PreferenceFragment {

    Handler handler = new Handler();

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);

        {
            final Context context = inflater.getContext();
            ViewGroup layout = (ViewGroup) view.findViewById(R.id.list_container);
            RecyclerView v = getListView();

            int fab_margin = (int) getResources().getDimension(R.dimen.fab_margin);
            int fab_size = ThemeUtils.dp2px(getActivity(), 61);
            int pad = 0;
            int top = 0;
            if (Build.VERSION.SDK_INT <= 16) { // so, it bugged only on 16
                pad = ThemeUtils.dp2px(context, 10);
                top = (int) getResources().getDimension(R.dimen.appbar_padding_top);
            }

            v.setClipToPadding(false);
            v.setPadding(pad, top, pad, pad + fab_size + fab_margin);

            FloatingActionButton fab = new FloatingActionButton(context);
            fab.setImageResource(R.drawable.ic_add_black_24dp);
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ContentFrameLayout.LayoutParams.WRAP_CONTENT, ContentFrameLayout.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.BOTTOM | Gravity.RIGHT;
            lp.setMargins(fab_margin, fab_margin, fab_margin, fab_margin);
            fab.setLayoutParams(lp);
            layout.addView(fab);

            // fix nexus 9 tabled bug, when fab showed offscreen
            handler.post(new Runnable() {
                @Override
                public void run() {
                    view.requestLayout();
                }
            });

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(permitted())
                        selectFile();
                }
            });
        }

        return view;
    }

    void selectFile() {
        EditDialogFragment dialog = new EditDialogFragment();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        dialog.show(((MainActivity) getActivity()).getSupportFragmentManager(), "");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (permitted(permissions))
                    selectFile();
                else
                    Toast.makeText(getActivity(), R.string.not_permitted, Toast.LENGTH_SHORT).show();
        }
    }

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE};

    boolean permitted(String[] ss) {
        for (String s : ss) {
            if (ContextCompat.checkSelfPermission(getActivity(), s) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    boolean permitted() {
        for (String s : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getActivity(), s) != PackageManager.PERMISSION_GRANTED) {
                FragmentCompat.requestPermissions(this, PERMISSIONS, 1);
                return false;
            }
        }
        return true;
    }
}
