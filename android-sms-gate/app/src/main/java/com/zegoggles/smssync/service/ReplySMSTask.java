package com.zegoggles.smssync.service;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.FetchProfile;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.store.XOAuth2AuthenticationFailedException;
import com.fsck.k9.mail.store.imap.ImapStore;
import com.github.axet.smsgate.activities.SendSMS;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.auth.TokenRefreshException;
import com.zegoggles.smssync.auth.TokenRefresher;
import com.zegoggles.smssync.mail.BackupImapStore;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.state.ReplyState;
import com.zegoggles.smssync.service.state.SmsSyncState;

import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zegoggles.smssync.App.TAG;
import static com.zegoggles.smssync.mail.DataType.SMS;
import static com.zegoggles.smssync.service.state.SmsSyncState.CALC;
import static com.zegoggles.smssync.service.state.SmsSyncState.CANCELED_RESTORE;
import static com.zegoggles.smssync.service.state.SmsSyncState.ERROR;
import static com.zegoggles.smssync.service.state.SmsSyncState.FINISHED_REPLYSMS;
import static com.zegoggles.smssync.service.state.SmsSyncState.LOGIN;
import static com.zegoggles.smssync.service.state.SmsSyncState.REPLYSMS;
import static com.zegoggles.smssync.service.state.SmsSyncState.UPDATING_THREADS;

class ReplySMSTask extends AsyncTask<BackupConfig, ReplyState, ReplyState> {
    private Set<String> smsIds = new HashSet<String>();
    private Set<String> callLogIds = new HashSet<String>();
    private Set<String> uids = new HashSet<String>();

    private final SmsReplyService service;
    private final ContentResolver resolver;
    private final TokenRefresher tokenRefresher;
    Preferences preferences;


    static class RefMap {
        // message id
        public String id;
        // incoming SMS phone number
        public String phone;
        // incomming SMS date
        public String date;
        // thread id
        public String thread;
        // groupped emails (replies)
        public List<ImapStore.ImapMessage> group = new ArrayList<ImapStore.ImapMessage>();

        public RefMap() {
        }

        public RefMap(ImapStore.ImapMessage m) throws MessagingException {
            id = m.getMessageId();

            if (this.phone == null) {
                // this refmap group has a key message with phone number
                String[] addrs = m.getHeader("X-smssync-address");
                if (addrs.length > 0) {
                    this.phone = addrs[0];
                }
            }

            if (this.date == null) {
                String[] dates = m.getHeader("X-smssync-date");
                if (dates.length > 0) {
                    this.date = dates[0];
                }
            }

            if (this.thread == null) {
                String[] thread = m.getHeader("X-smssync-thread");
                if (thread.length > 0) {
                    this.thread = thread[0];
                }
            }
        }

        public boolean isKey() {
            return phone != null;
        }

        public void apply(RefMap m) throws MessagingException {
            if (this.id == null) {
                this.id = m.id;
            }

            if (this.phone == null) {
                this.phone = m.phone;
            }

            if (this.date == null) {
                this.date = m.date;
            }

            if (this.thread == null) {
                this.thread = m.thread;
            }
        }

        public void add(ImapStore.ImapMessage m) throws MessagingException {
            group.add(m);
        }
    }

    static class FormattingVisitor implements NodeVisitor {
        private static final int maxWidth = 80;
        private int width = 0;
        private StringBuilder accum = new StringBuilder(); // holds the accumulated text

        // hit when the node is first seen
        public void head(Node node, int depth) {
            String name = node.nodeName();
            if (node instanceof TextNode)
                append(((TextNode) node).text()); // TextNodes carry all user-readable text in the DOM.
            else if (name.equals("li"))
                append("\n * ");
            else if (name.equals("dt"))
                append("  ");
            else if (StringUtil.in(name, "p", "h1", "h2", "h3", "h4", "h5", "tr", "div"))
                append("\n");
        }

        // hit when all of the node's children (if any) have been visited
        public void tail(Node node, int depth) {
            String name = node.nodeName();
            if (StringUtil.in(name, "br", "dd", "dt", "p", "h1", "h2", "h3", "h4", "h5"))
                append("\n");
            else if (name.equals("a"))
                append(String.format(" <%s>", node.absUrl("href")));
        }

        // appends text to the string builder with a simple word wrap method
        private void append(String text) {
            if (text.startsWith("\n"))
                width = 0; // reset counter if starts with a newline. only from formats above, not in natural text
            if (text.equals(" ") &&
                    (accum.length() == 0 || StringUtil.in(accum.substring(accum.length() - 1), " ", "\n")))
                return; // don't accumulate long runs of empty spaces

            if (text.length() + width > maxWidth) { // won't fit, needs to wrap
                String words[] = text.split("\\s+");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                    boolean last = i == words.length - 1;
                    if (!last) // insert a space if not the last word
                        word = word + " ";
                    if (word.length() + width > maxWidth) { // wrap and reset counter
                        accum.append("\n").append(word);
                        width = word.length();
                    } else {
                        accum.append(word);
                        width += word.length();
                    }
                }
            } else { // fits as is, without need to wrap text
                accum.append(text);
                width += text.length();
            }
        }

        @Override
        public String toString() {
            return accum.toString();
        }
    }

    public ReplySMSTask(SmsReplyService service,
                        ContentResolver resolver,
                        Preferences preferences,
                        TokenRefresher tokenRefresher) {
        this.service = service;
        this.resolver = resolver;
        this.preferences = preferences;
        this.tokenRefresher = tokenRefresher;
    }

    @Override
    protected void onPreExecute() {
        App.bus.register(this);
    }

    @Subscribe
    public void userCanceled(UserCanceled canceled) {
        cancel(false);
    }

    @NotNull
    protected ReplyState doInBackground(BackupConfig... params) {
        if (params == null || params.length == 0)
            throw new IllegalArgumentException("No config passed");
        BackupConfig config = params[0];

        try {
            service.acquireLocks();
            return restore(config);
        } finally {
            service.releaseLocks();
        }
    }

    static String extractMessage(String body) {
        String msg = body;

        // drop quotes. lines starting with: ">...."
        {
            Pattern p = Pattern.compile("^>.*$", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        // drop quotes. lines starting with: "<...."
        {
            Pattern p = Pattern.compile("^<.*$", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        // drop multi empty lines
        {
            Pattern p = Pattern.compile("^\\s*$\\n", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        {
            Pattern p = Pattern.compile("^\\s*", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        {
            Pattern p = Pattern.compile("\\s*$", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        msg = msg.trim();

        return msg;
    }

    String decodeBody(Body body) throws MessagingException, IOException {
        InputStream is = null;
        if (body instanceof MimeMultipart) {
            for (BodyPart bp : ((MimeMultipart) body).getBodyParts()) {
                if (bp.getContentType().contains("text/html")) {
                    is = MimeUtility.decodeBody(bp.getBody());
                    Document doc = Jsoup.parse(IOUtils.toString(is));
                    for (Element element : doc.select("blockquote")) {
                        element.remove();
                    }
                    for (Element element : doc.select(".gmail_quote")) {
                        element.remove();
                    }
                    FormattingVisitor formatter = new FormattingVisitor();
                    NodeTraversor traversor = new NodeTraversor(formatter);
                    traversor.traverse(doc);
                    return formatter.toString();
                }
            }
            if (is == null) {
                for (BodyPart bp : ((MimeMultipart) body).getBodyParts()) {
                    if (bp.getContentType().contains("text/plain")) {
                        is = bp.getBody().getInputStream();
                    }
                }
            }
        } else {
            is = MimeUtility.decodeBody(body);
        }
        if (is == null) {
            throw new MessagingException("body.getInputStream() is null for ");
        }
        return IOUtils.toString(is);
    }

    void deleteMessage(BackupImapStore imap, String id) throws MessagingException {
        for (Folder fn : imap.getPersonalNamespaces(false)) {
            BackupImapStore.BackupFolder f = imap.createAndOpenFolder(fn.getName());
            ImapStore.ImapMessage msg = f.getMessagesId(id);
            if (msg != null) {
                msg.setFlag(Flag.DELETED, true);
                f.expunge(msg.getUid());
            }
            f.close();
        }
    }

    private boolean smsExists(Date date, String phone) {
        return smsExists("" + date.getTime(), phone);
    }

    private boolean smsExists(String date, String phone) {
        // just assume equality on date+address+type
        Cursor c = resolver.query(Consts.SMS_PROVIDER,
                new String[]{"_id"},
                "date = ? AND address = ?",
                new String[]{
                        date,
                        phone
                },
                null
        );

        boolean exists = false;
        if (c != null) {
            exists = c.getCount() > 0;
            c.close();
        }
        return exists;
    }

    private ReplyState restore(BackupConfig config) {
        final BackupImapStore imapStore = config.imapStore;

        int currentRestoredItem = 0;
        try {
            publishProgress(LOGIN);
            imapStore.checkSettings();

//            K9MailLib.setDebug(true);
//            K9MailLib.setDebugSensitive(true);
//            K9MailLib.DEBUG_PROTOCOL_IMAP = true;

            publishProgress(CALC);

            BackupImapStore.BackupFolder folder = imapStore.getFolder(SMS);

            List<ImapStore.ImapMessage> msgs = folder.getMessagesDate(null);

            final int itemsToRestoreCount = msgs.size();

            Map<String, RefMap> map = new TreeMap<String, RefMap>();

            if (itemsToRestoreCount > 0) {
                for (; currentRestoredItem < itemsToRestoreCount && !isCancelled(); currentRestoredItem++) {
                    ImapStore.ImapMessage m = msgs.get(currentRestoredItem);

                    FetchProfile fp = new FetchProfile();
                    fp.add(FetchProfile.Item.BODY);

                    folder.fetch(Arrays.asList(m), fp, null);

                    RefMap rm = new RefMap(m);
                    if (rm.isKey()) {
                        RefMap rm2 = map.get(rm.id);
                        if (rm2 == null) {
                            map.put(rm.id, rm);
                        } else {
                            rm2.apply(rm);
                        }
                    } else {
                        String[] refs = m.getHeader("References");

                        for (String rr : refs) {
                            for (String r : rr.split("\n")) {
                                r = r.trim();
                                RefMap rm2 = map.get(r);
                                if (rm2 == null) {
                                    rm2 = new RefMap();
                                    map.put(r, rm2);
                                }
                                rm2.add(m);
                            }
                        }
                    }

                    publishProgress(new ReplyState(REPLYSMS, currentRestoredItem + 1, itemsToRestoreCount, config.backupType, null, null));
                }

                if (!isCancelled()) {
                    for (String key : map.keySet()) {
                        currentRestoredItem++;

                        RefMap m = map.get(key);

                        if (!m.isKey())
                            continue;

                        for (ImapStore.ImapMessage im : m.group) {
                            // skip key (incoming) message
                            if (im.getHeader("X-smssync-address").length > 0)
                                continue;

                            // mis expunge folder may have delted message
                            if (im.getFlags().contains(Flag.DELETED))
                                continue;

                            if (im.getFlags().contains(Flag.FORWARDED))
                                continue;

                            Date date = im.getSentDate();

                            Body body = im.getBody();

                            String b = decodeBody(body);

                            String msg = extractMessage(b);

                            if (msg != null && !msg.equals("")) {
                                // do not work with sms, not belong to current phone.
                                // here may be two phones with SMS-GATE applications.
                                // each phone must work with it own messages to keem phone number.
                                if (smsExists(m.date, m.phone)) {
                                    if (!smsExists(date, m.phone)) {
                                        SendSMS.sendSMS(service, m.phone, msg, date, m.thread);
                                        im.setFlag(Flag.FORWARDED, true);
                                        im.setFlag(Flag.SEEN, true);
//                                    Log.v(TAG, "----------------------------");
//                                    Log.v(TAG, m.phone + ":" + msg);
//                                    Log.v(TAG, "----------------------------");
                                    } else {
                                        Log.v(TAG, "skiping sms send, becase it already sent " + m.date + " " + m.phone);
                                    }
                                } else {
                                    Log.v(TAG, "skiping sms send, becase it not in INBOX " + m.date + " " + m.phone);
                                }
                            }
                        }

                        publishProgress(new ReplyState(REPLYSMS, currentRestoredItem, map.size(), config.backupType, null, null));
                    }

//                    for (Folder fn : imapStore.getPersonalNamespaces(false)) {
//                        BackupImapStore.BackupFolder f = imapStore.createAndOpenFolder(fn.getName());
//                        f.expungeSimple();
//                        f.close();
//                    }
                }

                if (!isCancelled()) {
                    publishProgress(UPDATING_THREADS);
                    updateAllThreads();
                }
            } else {
                Log.d(TAG, "nothing to restore");
            }

            return new ReplyState(FINISHED_REPLYSMS,
                    0,
                    0,
                    config.backupType, null, null);
        } catch (XOAuth2AuthenticationFailedException e) {
            return handleAuthError(config, currentRestoredItem, e);
        } catch (AuthenticationFailedException e) {
            return transition(ERROR, e);
        } catch (IOException e) {
            Log.e(TAG, "error", e);
            return transition(ERROR, e);
        } catch (MessagingException e) {
            Log.e(TAG, "error", e);
            return transition(ERROR, e);
        } catch (IllegalStateException e) {
            // usually memory problems (Couldn't init cursor window)
            return transition(ERROR, e);
        } finally {
            imapStore.closeFolders();
        }
    }

    private ReplyState handleAuthError(BackupConfig config, int currentRestoredItem, XOAuth2AuthenticationFailedException e) {
        if (e.getStatus() == 400) {
            Log.d(TAG, "need to perform xoauth2 token refresh");
            try {
                tokenRefresher.refreshOAuth2Token();
                // we got a new token, let's retry one more time - we need to pass in a new store object
                // since the auth params on it are immutable
                return restore(config.retryWithStore(service.getBackupImapStore()));
            } catch (MessagingException ignored) {
                Log.w(TAG, ignored);
            } catch (TokenRefreshException refreshException) {
                Log.w(TAG, refreshException);
            }
        } else {
            Log.w(TAG, "unexpected xoauth status code " + e.getStatus());
        }
        return transition(ERROR, e);
    }

    private void publishProgress(SmsSyncState smsSyncState) {
        publishProgress(smsSyncState, null);
    }

    private void publishProgress(SmsSyncState smsSyncState, Exception exception) {
        publishProgress(transition(smsSyncState, exception));
    }

    private ReplyState transition(SmsSyncState smsSyncState, Exception exception) {
        return service.getState().transition(smsSyncState, exception);
    }

    @Override
    protected void onPostExecute(ReplyState result) {
        if (result != null) {
            Log.d(TAG, "finished (" + result + "/" + uids.size() + ")");
            post(result);
        }
        App.bus.unregister(this);
    }

    @Override
    protected void onCancelled() {
        Log.d(TAG, "restore canceled by user");
        post(transition(CANCELED_RESTORE, null));
        App.bus.unregister(this);
    }

    @Override
    protected void onProgressUpdate(ReplyState... progress) {
        if (progress != null && progress.length > 0 && !isCancelled()) {
            post(progress[0]);
        }
    }

    private void post(ReplyState changed) {
        if (changed == null) return;
        App.bus.post(changed);
    }

    private void updateAllThreads() {
        // thread dates + states might be wrong, we need to force a full update
        // unfortunately there's no direct way to do that in the SDK, but passing a
        // negative conversation id to delete should to the trick
        Log.d(TAG, "updating threads");
        resolver.delete(Uri.parse("content://sms/conversations/-1"), null, null);
        Log.d(TAG, "finished");
    }

    protected Set<String> getSmsIds() {
        return smsIds;
    }
}
