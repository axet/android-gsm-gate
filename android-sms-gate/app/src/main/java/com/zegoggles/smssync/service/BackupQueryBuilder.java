package com.zegoggles.smssync.service;

import android.content.Context;
import android.net.Uri;
import android.provider.CallLog;

import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.MmsConsts;
import com.zegoggles.smssync.SmsConsts;
import com.zegoggles.smssync.mail.DataType;

import org.jetbrains.annotations.Nullable;

import java.util.Locale;

import static com.zegoggles.smssync.mail.DataType.MMS;
import static com.zegoggles.smssync.mail.DataType.SMS;

class BackupQueryBuilder {
    private final Context context;

    // only query for needed fields
    // http://stackoverflow.com/questions/12033234/get-calls-provider-internal-structure
    private static final String[] CALLLOG_PROJECTION = {
            CallLog.Calls._ID,
            CallLog.Calls.NUMBER,
            CallLog.Calls.DURATION,
            CallLog.Calls.DATE,
            CallLog.Calls.TYPE
    };

    public BackupQueryBuilder(Context context) {
        this.context = context;
    }

    static class Query {
        final Uri uri;
        final String[] projection;
        final String selection;
        final String[] selectionArgs;
        final String sortOrder;

        Query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
            this.uri = uri;
            this.projection = projection;
            this.selection = selection;
            this.selectionArgs = selectionArgs;
            this.sortOrder = sortOrder;
        }

        Query(Uri uri, String[] projection, String selection, String[] selectionArgs, int max) {
            this(uri, projection, selection, selectionArgs,
                    max > 0 ? SmsConsts.DATE + " LIMIT " + max : SmsConsts.DATE);
        }
    }

    public
    @Nullable
    Query buildQueryForDataType(DataType type, int max) {
        switch (type) {
            case SMS:
                return getQueryForSMS(max);
            case MMS:
                return getQueryForMMS(max);
            default:
                return null;
        }
    }

    public
    @Nullable
    Query buildMostRecentQueryForDataType(DataType type) {
        switch (type) {
            case MMS:
                return new Query(
                        Consts.MMS_PROVIDER,
                        new String[]{MmsConsts.DATE},
                        null,
                        null,
                        MmsConsts.DATE + " DESC LIMIT 1");
            case SMS:
                return new Query(
                        Consts.SMS_PROVIDER,
                        new String[]{SmsConsts.DATE},
                        SmsConsts.TYPE + " <> ?",
                        new String[]{String.valueOf(SmsConsts.MESSAGE_TYPE_DRAFT)},
                        SmsConsts.DATE + " DESC LIMIT 1");
            default:
                return null;
        }
    }

    private Query getQueryForSMS(int max) {
        return new Query(Consts.SMS_PROVIDER,
                null,
                String.format(Locale.ENGLISH,
                        "%s > ? AND %s == ? %s",
                        SmsConsts.DATE,
                        SmsConsts.TYPE,
                        "").trim(),
                new String[]{
                        String.valueOf(SMS.getMaxSyncedDate(context)),
                        String.valueOf(SmsConsts.MESSAGE_TYPE_INBOX)
                },
                max);
    }

    private Query getQueryForMMS(int max) {
        long maxSynced = MMS.getMaxSyncedDate(context);
        if (maxSynced > 0) {
            // NB: max synced date is stored in seconds since epoch in database
            maxSynced = (long) (maxSynced / 1000d);
        }
        return new Query(
                Consts.MMS_PROVIDER,
                null,
                String.format(Locale.ENGLISH, "%s > ? AND %s <> ? %s",
                        SmsConsts.DATE,
                        MmsConsts.TYPE,
                        "").trim(),
                new String[]{
                        String.valueOf(maxSynced),
                        MmsConsts.DELIVERY_REPORT
                },
                max);
    }
}
