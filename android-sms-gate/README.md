# SMS Gate
(based on https://github.com/jberkel/sms-backup-plus)

You should be able to control all data produced by phone and be able to collect it all into one place (cloud / p2p or local). SMS Gate app backup all SMS from Android/INCOMING folder to the Imap/INBOX folder (or Gmail) and allows you to reply to the SMS using standard reply e-mail mecanics.

Default SMS backup folder set to Imap/INBOX folder. All your incoming SMS will appear in INBOX as Unread messages. You can reply them, SMS Gate will check INBOX folder and send respond to the sender.

After repying, SMS Gate will mark as forwarded the reply email in your mailbox. SMS Gate will not reply to the incoming SMS it do not own (if you have two instances on two phones it helps do not cross sms reply functionality).

Works best with standalone android phones (https://github.com/axet/android-gsm-gate/tree/master/keneksi-sun)

## Services

  * SMS Backup have regular schedule set to 1 minute + Incoming SMS event trigs the backup.
  * SMS Reply monitors for Imap Push events read INBOX Imap folder. Have regular schedule 5 minutes.

## Links

  * https://github.com/axet/android-media-merger
  * https://pushjet.io/
