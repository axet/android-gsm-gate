# Concept

This project describes full concept how to create Android 5.0+ <-> Asterisk connection.

All we need is to put a little effort writing few code to make it real.

In few words:

* Android 5.0 <-> miniUSB <-> OpenWRT router <-> Asterisk

## OpenWRT router

Has to have few kernel/user space code which emulates devices next to USB OTG cable. Behind OTG cable should be possible to emulate two USB devices: USB Microphone, USB Speaker or USB headset/VOIP phone. All incoming SMS would be routed using Android APK installed from a market to the Asterisk using USB connection or TCP/IP. All audio traffic would be routed using Android OS USB audio devices (emulated by OpenWRT based router). Call answering can be done using send key events or calling system services or USB headset button.

## Links

  * https://code.google.com/p/android/issues/detail?id=24614
  * https://www.youtube.com/watch?v=6LCEYcKo-Vg
  * https://source.android.com/devices/audio/usb.html
  * [Using Android phone as GSM Gateway.pdf](docs/Using Android phone as GSM Gateway.pdf)
  * https://github.com/pelya/android-keyboard-gadget
  
